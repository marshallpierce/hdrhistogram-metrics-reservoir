import java.net.URI
import java.time.Duration

plugins {
    `java-library`
    `maven-publish`
    signing
    id("io.github.gradle-nexus.publish-plugin") version "1.0.0"
    id("me.champeau.gradle.jmh") version "0.5.3"
    id("net.researchgate.release") version "2.8.1"
    id("com.github.ben-manes.versions") version "0.38.0"
}

group = "org.mpierce.metrics.reservoir"

repositories {
    mavenCentral()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    withSourcesJar()
    withJavadocJar()
}

dependencies {
    testImplementation("junit", "junit", "4.13.2")
    implementation("com.google.code.findbugs", "jsr305", "3.0.2")
    api("io.dropwizard.metrics", "metrics-core", "4.1.12.1")
    api("org.hdrhistogram", "HdrHistogram", "2.1.12")
}

tasks {
    afterReleaseBuild {
        dependsOn(provider { project.tasks.named("publishToSonatype") })
    }
}

publishing {
    publications {
        register<MavenPublication>("sonatype") {
            from(components["java"])
            // sonatype required pom elements
            pom {
                name.set("${project.group}:${project.name}")
                description.set(name)
                url.set("https://bitbucket.org/marshallpierce/hdrhistogram-metrics-reservoir")
                licenses {
                    license {
                        name.set("Copyfree Open Innovation License 0.5")
                        url.set("https://copyfree.org/content/standard/licenses/coil/license.txt")
                    }
                }
                developers {
                    developer {
                        id.set("marshallpierce")
                        name.set("Marshall Pierce")
                        email.set("575695+marshallpierce@users.noreply.github.com")
                    }
                }
                scm {
                    connection.set("scm:git:https://bitbucket.org/marshallpierce/hdrhistogram-metrics-reservoir")
                    developerConnection.set("scm:git:ssh://git@bitbucket.org:marshallpierce/hdrhistogram-metrics-reservoir.git")
                    url.set("https://bitbucket.org/marshallpierce/hdrhistogram-metrics-reservoir")
                }
            }
        }
    }

    // A safe throw-away place to publish to:
    // ./gradlew publishSonatypePublicationToLocalDebugRepository -Pversion=foo
    repositories {
        maven {
            name = "localDebug"
            url = URI.create("file:///${project.buildDir}/repos/localDebug")
        }
    }
}


// don't barf for devs without signing set up
if (project.hasProperty("signing.keyId")) {
    signing {
        sign(project.extensions.getByType<PublishingExtension>().publications["sonatype"])
    }
}

nexusPublishing {
    repositories {
        sonatype {
            // sonatypeUsername and sonatypePassword properties are used automatically
            stagingProfileId.set("ab8c5618978d18") // org.mpierce
        }
    }
    // these are not strictly required. The default timeouts are set to 1 minute. But Sonatype can be really slow.
    // If you get the error "java.net.SocketTimeoutException: timeout", these lines will help.
    connectTimeout.set(Duration.ofMinutes(3))
    clientTimeout.set(Duration.ofMinutes(3))
}
